class AddUserIdToComments < ActiveRecord::Migration
  def change
    add_column :comments, :user_id, :integer
  end
  add_index :comments, [:micropost_id, :user_id, :created_at], unique: true
end
