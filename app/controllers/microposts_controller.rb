class MicropostsController < ApplicationController
  before_action :signed_in_user

  def create
    @micropost = current_user.microposts.build(micropost_params)
    if @micropost.save
      flash[:success] = "Micropost created!"
      redirect_to root_url
    else
      @feed_items = []
      render 'static_pages/home'
    end
  end

  def show
    @micropost = Micropost.find(params[:id])
    micropost_user = @micropost.user
    followers = micropost_user.followers
    @can_comment = false
    if followers.any?{|a| a.id == current_user.id} || micropost_user.id == current_user.id
        @can_comment = true
    end
    @comments = @micropost.comments.paginate(page: params[:page])
    @comment  = @micropost.comments.build
  end

  def destroy
  end

  private

    def micropost_params
      params.require(:micropost).permit(:content)
    end
end