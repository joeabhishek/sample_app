class CommentsController < ApplicationController
  before_action :signed_in_user

  def create
    micropost_id = params[:micropost]
    micropost = Micropost.find(micropost_id)
    user_id = current_user.id
    micropost_user = micropost.user
    followers = micropost_user.followed_users

    if comment_params[:content].empty?
      flash[:alert] = "Comment empty"
      redirect_to micropost_path(micropost_id)
    else
      @comment = Comment.new(content: comment_params[:content], micropost_id: micropost_id, user_id: user_id)
      if @comment.save
        flash[:success] = "Comment created!"
        redirect_to micropost_path(micropost_id)
      else
        flash[:alert] = "Comment could not be saved"
        redirect_to micropost_path(micropost_id)
      end
    end
  end

  def destroy
  end

  def show
  end

  private
    def comment_params
      params.require(:comment).permit(:content)
    end
end