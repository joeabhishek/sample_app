require 'spec_helper'

describe "Micropost pages" do

  subject { page }

  let(:user) { FactoryGirl.create(:user) }
  let!(:m1) { FactoryGirl.create(:micropost, user:user, content:"Foo") }
  let!(:comment) do
            FactoryGirl.create(:comment, micropost: m1, user: user, created_at: 1.day.ago)
  end
  
  before { sign_in user }

  describe "comment creation" do
    before do
    	user.save
    	m1.save
    	comment.save
      visit micropost_path(m1) 
    end

    describe "show display the micropost content" do
        it { should have_content(m1.content) }
    end 

    describe "with invalid information" do

      it "should not create a comment" do
        expect { click_button "Post" }.not_to change(Comment, :count)
      end
    end

    describe "with valid information" do

      before { fill_in 'comment_content', with: "Lorem ipsum" }
      it "should create a comment" do
        expect { click_button "Post" }.to change(Comment, :count).by(1)
      end
    end
  end
end