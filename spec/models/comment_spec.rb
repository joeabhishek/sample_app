require 'spec_helper'

describe Comment do

  let(:user) { FactoryGirl.create(:user) }
   let!(:m1) { FactoryGirl.create(:micropost, user:user, content:"Foo") }
  before do
    @comment = Comment.new(content: "FooBaz", micropost_id: m1.id, user_id: user.id)
  end

  subject { @comment }

  it { should respond_to(:content) }
  it { should respond_to(:micropost_id) }
  it { should respond_to(:micropost) }
  it { should respond_to(:user_id) }
  it { should respond_to(:user) }
  its(:micropost) { should eq m1 }
  its(:user) { should eq user }

  it { should be_valid }

  describe "when micropost_id is not present" do
    before { @comment.micropost_id = nil }
    it { should_not be_valid }
  end

  describe "with blank content" do
    before { @comment.content = " " }
    it { should_not be_valid }
  end

  describe "with content that is too long" do
    before { @comment.content = "a" * 141 }
    it { should_not be_valid }
  end

  describe "when user_id is not present" do
    before { @comment.user_id = nil }
    it { should_not be_valid }
  end 

end